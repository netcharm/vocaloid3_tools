﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace SingerAssistant
{   
    public enum SINGER_LANGUAGE { OTH=-1, JPN=0, ENG=1, KOR=2, ESP=3, CHS=4 };

    public class SingerInfo : ConfigurationSection
    {

    }

    public class singer_v3
    {
        /*
         * private member
         */
        
        /*
         * define properties
         */
        #region properties
        private String Singer_RegistryKey = "SOFTWARE\\VCLDASGN3\\DATABASE\\VOICE3";
        public String RegistryKey
        {
            get { return Singer_RegistryKey; }
            set { Singer_RegistryKey = value; }
        }

        private String Singer_DisplayName = String.Empty;
        public String DisplayName
        {
            get { return Singer_DisplayName; }
            set { Singer_DisplayName = value; }
        }

        private String Singer_Alias = String.Empty;
        public String Alias
        {
            get { return Singer_Alias; }
            set { Singer_Alias = value; }
        }

        private int Singer_Version = 3;
        public int Version
        {
            get { return Singer_Version; }
            set { Singer_Version = value; }
        }

        private SINGER_LANGUAGE Singer_Language = SINGER_LANGUAGE.JPN;
        public SINGER_LANGUAGE Language
        {
            get { return Singer_Language; }
            set { Singer_Language = value; }
        }

        private String Singer_Hashname = String.Empty;
        public String Hashname
        {
            get { return Singer_Hashname; }
            set { Singer_Hashname = value; }
        }

        private String Singer_InstallPath = String.Empty;
        public String Path
        {
            get { return Singer_InstallPath; }
            set { Singer_InstallPath = value; }
        }

        private String Singer_DRP = String.Empty;
        public String DRP
        {
            get { return Singer_DRP; }
            set { Singer_DRP = value; }
        }

        private Boolean Singer_Installed = false;
        public Boolean Installed
        {
            get { return Singer_Installed; }
            set { Singer_Installed = value; }
        }

        private String Singer_Name = String.Empty;
        public String Name
        {
            get { return Singer_Name; }
            set { Singer_Name = value; }
        }

        private String Singer_Time = String.Empty;
        public String Time
        {
            get { return Singer_Time; }
            set { Singer_Time = value; }
        }

        private String Singer_Keys = String.Empty;
        public String Keys
        {
            get { return Singer_Keys; }
            set { Singer_Keys = value; }
        }

        private String Singer_Intro = String.Empty;
        public String Intro
        {
            get { return Singer_Intro; }
            set { Singer_Intro = value; }
        }

        private Image Singer_Image = null;
        public Image Image
        {
            get { return Singer_Image; }
            set { Singer_Image = value; }
        }
        #endregion

        #region private members
        private Boolean RegistryRead( String Key, String Name, out String Value )
        {
            object result = null;
            
            RegistryKey SingerKey = Registry.LocalMachine.OpenSubKey( Key );
            if ( SingerKey != null )
            {
                result = SingerKey.GetValue( Name );
                SingerKey.Close();
            }

            if ( result != null )
            {
                Value = Convert.ToString( result );
                return true;
            }
            else
            {
                Value = String.Empty;
                return false;
            }
        }

        private Boolean RegistryRead( String Key, String Name, out uint Value )
        {
            object result = null;

            RegistryKey SingerKey = Registry.LocalMachine.OpenSubKey( Key );
            if ( SingerKey != null )
            {
                result = SingerKey.GetValue( Name );
                SingerKey.Close();
            }

            //if ( result != null ) Value = Convert.ToUInt32(result.ToString());
            if ( result != null )
            {
                Value = Convert.ToUInt32( result );
                return true;
            }
            else
            {
                Value = 0;
                return false;
            }
        }

        private Boolean RegistryRead( String Key, String Name, out Boolean Value )
        {
            object result = null;

            RegistryKey SingerKey = Registry.LocalMachine.OpenSubKey( Key );
            if ( SingerKey != null )
            {
                result = SingerKey.GetValue( Name );
                SingerKey.Close();
            }

            if ( result != null )
            {
                Value = Convert.ToBoolean(Convert.ToUInt32( result ));
                return true;
            }
            else
            {
                Value = false;
                return false;
            }
        }

        private Boolean RegistryWrite( String Key, String Name, String Value )
        {
            RegistryKey SingerKey = Registry.LocalMachine.OpenSubKey( Key, true );
            RegistryKey SingerKeyWow = Registry.LocalMachine.OpenSubKey( Key.Replace( "SOFTWARE\\VCLDASGN3", "SOFTWARE\\Wow6432Node\\VCLDASGN3" ), true );
            if ( ( SingerKey != null ) && ( SingerKeyWow != null ) )
            {
                SingerKey.SetValue( Name, Value, RegistryValueKind.String );
                SingerKeyWow.SetValue( Name, Value, RegistryValueKind.String );
                SingerKey.Close();
                SingerKeyWow.Close();
                return true;
            }
            return false;
        }

        private Boolean RegistryWrite( String Key, String Name, ulong Value )
        {
            RegistryKey SingerKey = Registry.LocalMachine.OpenSubKey( Key, true );
            RegistryKey SingerKeyWow = Registry.LocalMachine.OpenSubKey( Key.Replace("SOFTWARE\\VCLDASGN3", "SOFTWARE\\Wow6432Node\\VCLDASGN3"), true );
            if ( ( SingerKey != null ) && ( SingerKeyWow != null ) )
            {
                SingerKey.SetValue( Name, Value, RegistryValueKind.DWord );
                SingerKeyWow.SetValue( Name, Value, RegistryValueKind.DWord );
                SingerKey.Close();
                SingerKeyWow.Close();
                return true;
            }
            return false;
        }

        private Boolean RegistryWrite( String Key, String Name, Boolean Value )
        {
            RegistryKey SingerKey = Registry.LocalMachine.OpenSubKey( Key, true );
            RegistryKey SingerKeyWow = Registry.LocalMachine.OpenSubKey( Key.Replace( "SOFTWARE\\VCLDASGN3", "SOFTWARE\\Wow6432Node\\VCLDASGN3" ), true );
            if ( ( SingerKey != null ) && ( SingerKeyWow != null ) )
            {
                if ( Value )
                {
                    SingerKey.SetValue( Name, 1, RegistryValueKind.DWord );
                    SingerKeyWow.SetValue( Name, 1, RegistryValueKind.DWord );
                }
                else
                {
                    SingerKey.SetValue( Name, 0, RegistryValueKind.DWord );
                    SingerKeyWow.SetValue( Name, 0, RegistryValueKind.DWord );
                }
                SingerKey.Close();
                SingerKeyWow.Close();
                return true;
            }
            return false;
        }

        private Boolean RegistryClean( String Key, String Name )
        {
            RegistryKey SingerKey = Registry.LocalMachine.OpenSubKey( Key, true );
            RegistryKey SingerKeyWow = Registry.LocalMachine.OpenSubKey( Key.Replace( "SOFTWARE\\VCLDASGN3", "SOFTWARE\\Wow6432Node\\VCLDASGN3" ), true );
            if ( ( SingerKey != null ) && ( SingerKeyWow != null ) )
            {
                SingerKey.DeleteSubKeyTree( Name );
                SingerKeyWow.DeleteSubKeyTree( Name );
                SingerKey.Close();
                SingerKeyWow.Close();
                return true;
            }
            return false;
        }

        #endregion

        #region public members
        public Boolean GetInfo()
        {
            String ValueStr = String.Empty;
            uint ValueInt = 0;

            char[] charsToTrim = { ',', '.', ' ', '\\' };
            String Key = String.Format( "{0}\\{1}", this.RegistryKey.TrimEnd( charsToTrim ), this.Singer_Hashname );

            if ( this.RegistryRead( Key, "PATH", out ValueStr ) )
            {
                this.Singer_InstallPath = System.IO.Path.GetDirectoryName(ValueStr+"\\").Replace("\\\\", "\\");
            }
            if ( this.RegistryRead( Key, "NAME", out ValueStr ) )
            {
                this.Singer_Name = ValueStr;
            }
            if ( this.RegistryRead( Key, "TIME", out ValueStr ) )
            {
                this.Singer_Time = ValueStr;
            }
            if ( this.RegistryRead( Key, "DRP", out ValueStr ) )
            {
                this.Singer_DRP = ValueStr;
            }
            if ( this.RegistryRead( Key, "INSTALLED", out ValueInt ) )
            {
                if ( ValueInt == 1 ) this.Singer_Installed = true;
                else this.Singer_Installed = false;
            }
            if ( this.RegistryRead( Key + "\\Keys", "default", out ValueStr ) )
            {
                this.Singer_Keys = ValueStr;
            }
            return true;
        }
        public Boolean Install()
        {
            char[] charsToTrim = {',', '.', ' ', '\\'};
            String Key = String.Format( "{0}\\{1}", this.RegistryKey.TrimEnd( charsToTrim ), this.Singer_Hashname );

            if ( !String.IsNullOrEmpty( Key ) )
            {
                if ( !String.IsNullOrEmpty( this.Singer_InstallPath ) )
                {
                    this.RegistryWrite( Key, "PATH", this.Singer_InstallPath );
                }
                if ( !String.IsNullOrEmpty( this.Singer_Name ) )
                {
                    this.RegistryWrite( Key, "NAME", this.Singer_Name );
                }
                //if ( this.Singer_Installed != null )
                {
                    this.RegistryWrite( Key, "INSTALLED", true );
                    this.Singer_Installed = true;
                }
                if ( !String.IsNullOrEmpty( this.Singer_Time ) )
                {
                    this.RegistryWrite( Key, "TIME", this.Singer_Time );
                }
                if ( !String.IsNullOrEmpty(this.Singer_DRP) )
                {
                    this.RegistryWrite( Key, "DRP", this.Singer_DRP );
                }
                if ( !String.IsNullOrEmpty(this.Singer_Keys) )
                {
                    this.RegistryWrite( Key + "\\Keys", "default", this.Singer_Keys );
                }
                //if ( this.Singer_Language != null )
                {
                    Key = "SOFTWARE\\VCLDASGN3\\COMMON\\LANGUAGE\\";
                    switch ( this.Singer_Language )
                    {
                        case SINGER_LANGUAGE.JPN:
                            this.RegistryWrite( Key + "\\0", "g2pa", "g2pa3_JPN.dll" );
                            break;
                        case SINGER_LANGUAGE.ENG:
                            this.RegistryWrite( Key + "\\1", "g2pa", "g2pa3_ENG.dll" );
                            this.RegistryWrite( Key + "\\1", "udm", "udm3_ENG.dll" );
                            break;
                        case SINGER_LANGUAGE.KOR:
                            this.RegistryWrite( Key + "\\2", "g2pa", "g2pa3_KOR.dll" );
                            break;
                        case SINGER_LANGUAGE.ESP:
                            this.RegistryWrite( Key + "\\3", "g2pa", "g2pa3_ESP.dll" );
                            break;
                        case SINGER_LANGUAGE.CHS:
                            this.RegistryWrite( Key + "\\4", "g2pa", "g2pa3_CHS.dll" );
                            break;
                    }
                }
                return true;
            }
            return false;
        }

        public Boolean Uninstall()
        {
            char[] charsToTrim = { ',', '.', ' ', '\\' };
            String Key = String.Format( "{0}\\{1}", this.RegistryKey.TrimEnd( charsToTrim ), this.Singer_Hashname );

            if ( !String.IsNullOrEmpty( Key ) )
            {
                if ( !String.IsNullOrEmpty( this.Singer_InstallPath ) )
                {
                    this.RegistryWrite( Key, "PATH", this.Singer_InstallPath );
                }
                if ( !String.IsNullOrEmpty( this.Singer_Name ) )
                {
                    this.RegistryWrite( Key, "NAME", this.Singer_Name );
                }
                //if ( this.Singer_Installed != null )
                {
                    this.RegistryWrite( Key, "INSTALLED", false );
                    this.Singer_Installed = false;
                }
                if ( !String.IsNullOrEmpty( this.Singer_Time ) )
                {
                    this.RegistryWrite( Key, "TIME", this.Singer_Time );
                }
                if ( !String.IsNullOrEmpty( this.Singer_DRP ) )
                {
                    this.RegistryWrite( Key, "DRP", this.Singer_DRP );
                }
                if ( !String.IsNullOrEmpty( this.Singer_Keys ) )
                {
                    this.RegistryWrite( Key + "\\Keys", "default", this.Singer_Keys );
                }
                return true;
            }
            return false;
        }

        public Boolean Clean()
        {
            this.Singer_Installed = false;

            char[] charsToTrim = { ',', '.', ' ', '\\' };
            String Key = String.Format( "{0}\\{1}", this.RegistryKey.TrimEnd( charsToTrim ), this.Singer_Hashname );

            if ( !String.IsNullOrEmpty( Key ) )
            {
                return RegistryClean( Key, this.Singer_Hashname );
            }
            return false;
        }
        #endregion
    }

    public class singers
    {
        private static Dictionary<String, String> SingersNameTable = new Dictionary<String, String>();

        private static String AppName = System.Reflection.Assembly.GetEntryAssembly().Location;
        private static String AppPath = System.AppDomain.CurrentDomain.BaseDirectory;
        private static String AppFolder
        {
            get { return AppPath; }
            //set { AppPath = value; }
        }

        private static String VoiceDB_Path = System.AppDomain.CurrentDomain.BaseDirectory;
        public static String VoiceDB
        {
            get { return VoiceDB_Path; }
            set { VoiceDB_Path = value; }
        }

        private static List<singer_v3> Local_Singers = new List<singer_v3>();
        public static List<singer_v3> MySingers
        {
            get { return Local_Singers; }
        }

        private static Boolean FetchSingerInfo( singer_v3 singer )
        {
            String singerName = String.Format( "{0}\\{1}.info", singer.Path, singer.Hashname );
            if ( File.Exists( singerName) )
            {
                ExeConfigurationFileMap map = new ExeConfigurationFileMap();
                map.ExeConfigFilename = singerName;
                try
                {
                    Configuration config = ConfigurationManager.OpenMappedExeConfiguration( map, ConfigurationUserLevel.None );

                    AppSettingsSection appSection = config.AppSettings;

                    if ( appSection.Settings["RegistryKey"] != null )
                    {
                        singer.RegistryKey = appSection.Settings["RegistryKey"].Value;
                    }
                    if ( appSection.Settings["Version"] != null )
                    {
                        singer.Version = Convert.ToInt32( appSection.Settings["Version"].Value );
                    }
                    if ( appSection.Settings["Hashname"] != null )
                    {
                        singer.Hashname = appSection.Settings["Hashname"].Value;
                    }
                    if ( appSection.Settings["DisplayName"] != null )
                    {
                        singer.DisplayName = appSection.Settings["DisplayName"].Value;
                    }
                    if ( appSection.Settings["Alias"] != null )
                    {
                        if ( !String.IsNullOrEmpty( appSection.Settings["Alias"].Value ) )
                        {
                            singer.Alias = appSection.Settings["Alias"].Value;
                        }
                    }
                    if ( appSection.Settings["Language"] != null )
                    {
                        singer.Language = (SINGER_LANGUAGE)Enum.Parse( typeof( SINGER_LANGUAGE ), appSection.Settings["Language"].Value, true );
                    }
                    if ( appSection.Settings["Installed"] != null )
                    {
                        singer.Installed = Convert.ToBoolean( appSection.Settings["Installed"].Value );
                    }

                    if ( appSection.Settings["Name"] != null )
                    {
                        singer.Name = appSection.Settings["Name"].Value;
                    }
                    if ( appSection.Settings["Path"] != null )
                    {
                        String folder = appSection.Settings["Path"].Value;
                        if ( folder.StartsWith( "\\" ) ) folder = folder.Remove( 0, 1 );
                        if ( folder.EndsWith( "\\" ) ) folder = folder.Remove( folder.Length-1 );
                        if ( Path.IsPathRooted( folder ) )
                        {
                            singer.Path = folder;
                        }
                        else
                        {
                            singer.Path = String.Format("{0}\\{1}", VoiceDB, folder).Replace("\\\\", "\\");
                        }
                    }
                    if ( appSection.Settings["Time"] != null )
                    {
                        if ( !String.IsNullOrEmpty( appSection.Settings["Time"].Value ) )
                        {
                            singer.Time = appSection.Settings["Time"].Value;
                        }
                    }
                    if ( appSection.Settings["DRP"] != null )
                    {
                        if ( !String.IsNullOrEmpty( appSection.Settings["DRP"].Value ) )
                        {
                            singer.DRP = appSection.Settings["DRP"].Value;
                        }
                    }
                    if ( appSection.Settings["Keys"] != null )
                    {
                        if ( !String.IsNullOrEmpty( appSection.Settings["Keys"].Value ) )
                        {
                            singer.Keys = appSection.Settings["Keys"].Value;
                        }
                    }

                    if ( appSection.Settings["Image"] != null )
                    {
                        String imageName = appSection.Settings["Image"].Value;
                        if ( Path.IsPathRooted( imageName ) )
                        {
                            singer.Image = FetchSingerImage( imageName, false );
                        }
                        else
                        {
                            imageName = String.Format( "{0}\\{1}", singer.Path, imageName );
                            singer.Image = FetchSingerImage( imageName, false );
                        }
                    }
                    if ( appSection.Settings["Intro"] != null )
                    {
                        singer.Intro = appSection.Settings["Intro"].Value;
                    }
                    return true;
                }
                catch ( Exception ee )
                {
                    MessageBox.Show( String.Format( "{0}", ee.Message.ToString() ), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error );                        
                }
            }
            return false;
        }

        private static Image FetchSingerImage( String Name, Boolean AutoDetect = true )
        {
            if ( AutoDetect )
            {
                String imageNameJpg = String.Format( "{0}\\{1}\\{2}.jpg", AppPath, "_singerface", Path.GetFileName( Name ) );
                String imageNamePng = String.Format( "{0}\\{1}\\{2}.png", AppPath, "_singerface", Path.GetFileName( Name ) );
                String imageNameBmp = String.Format( "{0}\\{1}\\{2}.bmp", AppPath, "_singerface", Path.GetFileName( Name ) );
                String imageNameGif = String.Format( "{0}\\{1}\\{2}.png", AppPath, "_singerface", Path.GetFileName( Name ) );
                if ( File.Exists( imageNameJpg ) )
                {
                    return Image.FromFile( imageNameJpg );
                }
                else if ( File.Exists( imageNamePng ) )
                {
                    return Image.FromFile( imageNamePng );
                }
                else if ( File.Exists( imageNameBmp ) )
                {
                    return Image.FromFile( imageNameBmp );
                }
                else if ( File.Exists( imageNameGif ) )
                {
                    return Image.FromFile( imageNamePng );
                }
            }
            else
            {
                if ( File.Exists( Name ) )
                {
                    return Image.FromFile( Name );
                }

            }
            return null;
        }

        private static Image FetchSingerImage( singer_v3 Singer, Boolean AutoDetect = true )
        {
            String Name = Singer.Name;

            if ( AutoDetect )
            {
                String imageNameJpg = String.Format( "{0}\\{1}\\{2}.jpg", AppPath, "_singerface", Path.GetFileName( Name ) );
                String imageNamePng = String.Format( "{0}\\{1}\\{2}.png", AppPath, "_singerface", Path.GetFileName( Name ) );
                String imageNameBmp = String.Format( "{0}\\{1}\\{2}.bmp", AppPath, "_singerface", Path.GetFileName( Name ) );
                String imageNameGif = String.Format( "{0}\\{1}\\{2}.png", AppPath, "_singerface", Path.GetFileName( Name ) );
                if ( File.Exists( imageNameJpg ) )
                {
                    return Image.FromFile( imageNameJpg );
                }
                else if ( File.Exists( imageNamePng ) )
                {
                    return Image.FromFile( imageNamePng );
                }
                else if ( File.Exists( imageNameBmp ) )
                {
                    return Image.FromFile( imageNameBmp );
                }
                else if ( File.Exists( imageNameGif ) )
                {
                    return Image.FromFile( imageNamePng );
                }
            }
            else
            {
                String imageName = Name;
                if ( !Path.IsPathRooted( Name ) )
                {
                    imageName = String.Format( "{0}\\{1}", Singer.Path, Name );
                }
                if ( File.Exists( imageName ) )
                {
                    return Image.FromFile( imageName );
                }

            }
            return null;
        }

        private static List<singer_v3> GetLocalSingers()
        {
            List<singer_v3> mysingers = new List<singer_v3>();

            DirectoryInfo directoryInfo = new DirectoryInfo(VoiceDB_Path);
            try
            {
                foreach (DirectoryInfo childDirectoryInfo in directoryInfo.GetDirectories())
                {
                    String hashname = String.Empty;

                    String childDirName = childDirectoryInfo.Name.ToString();

                    foreach ( DirectoryInfo childDir in childDirectoryInfo.GetDirectories() )
                    {
                        singer_v3 singer = new singer_v3();
                            
                        singer.Hashname = childDir.Name.ToString();
                        singer.Path = String.Format( "{0}\\{1}", Path.GetDirectoryName( VoiceDB_Path + "\\" ).Replace( "\\\\", "\\" ), childDirName );
                        if ( FetchSingerInfo( singer ) )
                        {
                            singer.GetInfo();
                            mysingers.Add( singer );
                        }
                    }
                }
            }
            catch ( System.IO.DirectoryNotFoundException ee )
            {
                MessageBox.Show( String.Format( "{0}", ee.Message.ToString() ), "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning );                        
            }
            return mysingers;
        }

        /*
        #region unused code
        private static void InitSingersNameTable()
        {
            SingersNameTable.Clear();
            SingersNameTable.Add("BC7DD6RZMSSH2CB5","Lily_V3");
            SingersNameTable.Add("BCBG86S4FSYMTCBK","CUL");
            SingersNameTable.Add("BCCDC6XZLSZHZCB4","Megpoid_Sweet");
            SingersNameTable.Add("BCXDC6CZLSZHZCB4","VY2V3");
            SingersNameTable.Add("BDRE87E2FTTKTDBA","VY1V3");
            SingersNameTable.Add("BDSEB7L2KTWKYDC5","VY2V3_falsetto");
            SingersNameTable.Add("BETDB8W6KWZPYEB9","Tianyi_CHN");
            SingersNameTable.Add("BEYMB898KW2SYED9","Lapis");
            SingersNameTable.Add("BFZL99Y7GX3RWFC9","Mew");
            SingersNameTable.Add("BGADBAGZKYXHYGA2","Megpoid_Power");
            SingersNameTable.Add("BH9N4BH9BZWTNHAB","Gackpoid_Whisper");
            SingersNameTable.Add("BK7FFCS3P2SL4KB6","Gackpoid_Power");
            SingersNameTable.Add("BKCDCC9ZL2ZHZKC2","Megpoid_Native");
            SingersNameTable.Add("BKNECCWZL2ZHZKC2","Clara");
            SingersNameTable.Add("BL6M9DM8G3RSWLBL","Megpoid_Whisper");
            SingersNameTable.Add("BLCX9DC8G3RSWLBL","Oliver");
            SingersNameTable.Add("BLMGDDS4M3WM2LC6","Galaco");
            SingersNameTable.Add("BLRGDDR4M3WM2LC6","IA");
            SingersNameTable.Add("BM8K9EM6G4RPWMB3","SeeU_SV01_JPN");
            SingersNameTable.Add("BM8N7E49E4TTSMCL","Lily_Native");
            SingersNameTable.Add("BMFL9EC6G4RPWMB3","Bruno");
            SingersNameTable.Add("BMGK9EC6G4RPWMB3","Yukari");
            SingersNameTable.Add("BMTKDET6M4XP2MCC","Rion");
            SingersNameTable.Add("BP9GAGA4H6WMXP9D","Gackpoid_Native");
            SingersNameTable.Add("BX77CNBZLBPHZX97","SeeU_SV01_KOR");
            SingersNameTable.Add("BYACFPCYPCXG4Y9E","Megpoid_Adult");
        }

        private static void InitSingersInfo()
        {
            foreach ( singer_v3 singer in Local_Singers )
            {
                FetchSingerInfo( singer );
            }                
        }

        private static void LoadSingersInfo()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration( AppName );
            //AppSettingsSection appSection = config.Sections.Add("Singers", config);
            AppSettingsSection appSection = config.AppSettings;

            if ( appSection.Settings["Singer"] != null )
            {
                //VoiceDB_Path = appSection.Settings["Singer"].Value;
                //singers.VoiceDB = VoiceDB_Path;
                //singers.Scan();
            }
            else
            {
                //appSection.Settings.Add( "Singer", singers.VoiceDB );
                //config.Save( ConfigurationSaveMode.Full );
            }
        }

        private static void SaveSingersInfo()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration( AppName );
            AppSettingsSection appSection = config.AppSettings;

            if ( appSection.Settings["Singer"] != null )
            {
                //VoiceDB_Path = edVoiceDBFolder.Text;
                //appSection.Settings["VoiceDB_Path"].Value = VoiceDB_Path;
                //singers.VoiceDB = VoiceDB_Path;
                //singers.Scan();
            }
            else
            {
                //appSection.Settings.Add( "Singer", singers.VoiceDB );
            }
            config.Save( ConfigurationSaveMode.Full );
        }
        #endregion
        */

        public static void Scan()
        {
            Local_Singers = GetLocalSingers();
        }

        static singers()
        {
            //InitSingersNameTable();
            //Local_Singers = GetLocalSingers();
        }
    }
}
