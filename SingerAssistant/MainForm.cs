﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

namespace SingerAssistant
{
    public partial class MainForm : Form
    {
        //private String AppPath = System.Reflection.Assembly.GetEntryAssembly().Location;
        private String AppPath = System.AppDomain.CurrentDomain.BaseDirectory;
        private String VoiceDB_Path = System.AppDomain.CurrentDomain.BaseDirectory;

        private void LoadSetting()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration( Application.ExecutablePath );
            AppSettingsSection appSection = config.AppSettings;

            if ( appSection.Settings["VoiceDB_Path"] != null )
            {
                VoiceDB_Path = appSection.Settings["VoiceDB_Path"].Value;
            }
            else
            {
                appSection.Settings.Add( "VoiceDB_Path", singers.VoiceDB );
                config.Save( ConfigurationSaveMode.Full );
            }
            singers.VoiceDB = VoiceDB_Path;
            singers.Scan();
        }

        private void SaveSetting()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration( Application.ExecutablePath );
            AppSettingsSection appSection = config.AppSettings;

            if ( appSection.Settings["VoiceDB_Path"] != null )
            {
                VoiceDB_Path = edVoiceDBFolder.Text;
                appSection.Settings["VoiceDB_Path"].Value = VoiceDB_Path;
            }
            else
            {
                appSection.Settings.Add( "VoiceDB_Path", singers.VoiceDB );
            }
            config.Save( ConfigurationSaveMode.Full );
            singers.VoiceDB = VoiceDB_Path;
            singers.Scan();
        }

        private singer_v3 FindSinger( String Name )
        {            
            foreach ( singer_v3 singer in singers.MySingers )
            {
                if ( String.Compare( singer.DisplayName, Name, StringComparison.InvariantCultureIgnoreCase ) == 0 )
                {
                    return singer;
                }
            }
            return null;
        }

        private void RefreshSingerList()
        {
            TreeNode[] nodes_v2 = tvSingers.Nodes.Find( "V2", false );
            TreeNode[] nodes_v3 = tvSingers.Nodes.Find( "V3", false );
            if ( ( nodes_v3.Length > 0 ) && ( nodes_v2.Length > 0 ) )
            {
                TreeNode node_v2 = nodes_v2[0];
                TreeNode node_v3 = nodes_v3[0];
                node_v2.Nodes.Clear();
                node_v3.Nodes.Clear();
                foreach ( singer_v3 singer in singers.MySingers )
                {
                    TreeNode singernode = new TreeNode();
                    singernode.Text = String.Format( "{0}", singer.DisplayName );
                    singernode.Checked = singer.Installed;
                    switch(singer.Version)
                    {
                        case 2: 
                            node_v2.Nodes.Add( singernode );
                            break;
                        case 3:
                            node_v3.Nodes.Add( singernode );
                            break;
                    }
                }
                node_v2.Expand();
                node_v3.Expand();
            }
        }

        private void RefreshSingerInfo(singer_v3 singer)
        {
            if ( singer != null )
            {
                lvSingerInfo.BeginUpdate();
                lvSingerInfo.Items.Clear();

                ListViewItem item = null;

                item = new ListViewItem();
                item.Text = "DisplayName";
                item.SubItems.Add( singer.DisplayName );
                lvSingerInfo.Items.Add( item );

                item = new ListViewItem();
                item.Text = "HashName";
                item.SubItems.Add( singer.Hashname );
                lvSingerInfo.Items.Add( item );

                item = new ListViewItem();
                item.Text = "Name";
                item.SubItems.Add( singer.Name );
                lvSingerInfo.Items.Add( item );

                item = new ListViewItem();
                item.Text = "Path";
                item.SubItems.Add( singer.Path );
                lvSingerInfo.Items.Add( item );

                item = new ListViewItem();
                item.Text = "Time";
                item.SubItems.Add( singer.Time );
                lvSingerInfo.Items.Add( item );

                item = new ListViewItem();
                item.Text = "DRP";
                item.SubItems.Add( singer.DRP );
                lvSingerInfo.Items.Add( item );

                item = new ListViewItem();
                item.Text = "Installed";
                item.SubItems.Add( singer.Installed.ToString() );
                lvSingerInfo.Items.Add( item );

                item = new ListViewItem();
                item.Text = "Language";
                item.SubItems.Add( singer.Language.ToString() );
                lvSingerInfo.Items.Add( item );

                item = new ListViewItem();
                item.Text = "Keys";
                item.SubItems.Add( singer.Keys );
                lvSingerInfo.Items.Add( item );

                item = new ListViewItem();
                item.Text = "Alias";
                item.SubItems.Add( singer.Alias );
                lvSingerInfo.Items.Add( item );

                edSingerIntro.Text = singer.Intro;

                imgSinger.Image = singer.Image;

                if ( lvSingerInfo.Columns.Count == 2 )
                {
                    lvSingerInfo.Columns[0].AutoResize( ColumnHeaderAutoResizeStyle.ColumnContent );
                    lvSingerInfo.Columns[1].AutoResize( ColumnHeaderAutoResizeStyle.ColumnContent );
                }
                lvSingerInfo.EndUpdate();
            }
        }

        public MainForm()
        {
            InitializeComponent();
            this.Icon = Icon.ExtractAssociatedIcon( Application.ExecutablePath );
            LoadSetting();
        }

        private void MainForm_Load( object sender, EventArgs e )
        {
            if ( lvSingerInfo.Columns.Count == 0 )
            {
                lvSingerInfo.Columns.Add( new ColumnHeader().Text = "Item" );
                lvSingerInfo.Columns.Add( new ColumnHeader().Text = "Value" );
            }
            edVoiceDBFolder.Text = VoiceDB_Path;
            RefreshSingerList();
        }

        private void tvSingers_AfterSelect( object sender, TreeViewEventArgs e )
        {
            if ( tvSingers.SelectedNode != null )
            {
                singer_v3 singer = FindSinger( tvSingers.SelectedNode.Text );
                RefreshSingerInfo( singer );
            }
        }

        private void tvSingers_AfterCheck( object sender, TreeViewEventArgs e )
        {
            singer_v3 singer = FindSinger( e.Node.Text );
            if ( singer != null )
            {
                if ( e.Node.Checked )
                {
                    singer.Install();
                }
                else
                {
                    //singer.Uninstall();
                    singer.Clean();
                }
                tvSingers.SelectedNode = e.Node;
                RefreshSingerInfo( singer );
            }
        }

        private void btnSetVoiceDBFolder_Click( object sender, EventArgs e )
        {
            if ( String.Compare( edVoiceDBFolder.Text, singers.VoiceDB, StringComparison.InvariantCultureIgnoreCase ) != 0 )
            {
                SaveSetting();
            }
            else
            {
                singers.Scan();
            }
            RefreshSingerList();
        }

        private void btnInstall_Click( object sender, EventArgs e )
        {
            if ( tvSingers.SelectedNode != null )
            {
                singer_v3 singer = FindSinger( tvSingers.SelectedNode.Text );
                if ( singer != null )
                {
                    singer.Install();
                    RefreshSingerInfo( singer );
                }
            }
        }

        private void btnUninstall_Click( object sender, EventArgs e )
        {
            if ( tvSingers.SelectedNode != null )
            {
                singer_v3 singer = FindSinger( tvSingers.SelectedNode.Text );
                if ( singer != null )
                {
                    singer.Uninstall();
                    //singer.Clean();
                    RefreshSingerInfo( singer );
                }
            }
        }

        private void btnInstallAll_Click( object sender, EventArgs e )
        {          
            foreach ( singer_v3 singer in singers.MySingers )
            {
                if ( singer != null )
                {
                    singer.Install();
                }
            }
            if ( tvSingers.SelectedNode != null )
            {
                singer_v3 singer = FindSinger( tvSingers.SelectedNode.Text );
                RefreshSingerInfo( singer );
            }
        }

        private void btnUninstallAll_Click( object sender, EventArgs e )
        {
            foreach ( singer_v3 singer in singers.MySingers )
            {
                if ( singer != null )
                {
                    singer.Uninstall();
                }
            }
            if ( tvSingers.SelectedNode != null )
            {
                singer_v3 singer = FindSinger( tvSingers.SelectedNode.Text );
                RefreshSingerInfo( singer );
            }
        }

        private void btnClean_Click( object sender, EventArgs e )
        {
            if ( tvSingers.SelectedNode != null )
            {
                singer_v3 singer = FindSinger( tvSingers.SelectedNode.Text );
                if ( singer != null )
                {
                    singer.Clean();
                    RefreshSingerInfo( singer );
                }
            }
        }

        private void btnCleanAll_Click( object sender, EventArgs e )
        {
            foreach ( singer_v3 singer in singers.MySingers )
            {
                if ( singer != null )
                {
                    singer.Clean();
                }
            }
            if ( tvSingers.SelectedNode != null )
            {
                singer_v3 singer = FindSinger( tvSingers.SelectedNode.Text );
                RefreshSingerInfo( singer );
            }
        }
    }
}
