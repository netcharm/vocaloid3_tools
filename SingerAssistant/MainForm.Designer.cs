﻿namespace SingerAssistant
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("V2");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("V3");
            this.lvSingerInfo = new System.Windows.Forms.ListView();
            this.edVoiceDBFolder = new System.Windows.Forms.TextBox();
            this.btnSetVoiceDBFolder = new System.Windows.Forms.Button();
            this.tvSingers = new System.Windows.Forms.TreeView();
            this.btnInstall = new System.Windows.Forms.Button();
            this.btnUninstall = new System.Windows.Forms.Button();
            this.btnClean = new System.Windows.Forms.Button();
            this.btnCleanAll = new System.Windows.Forms.Button();
            this.btnInstallAll = new System.Windows.Forms.Button();
            this.btnUninstallAll = new System.Windows.Forms.Button();
            this.edSingerIntro = new System.Windows.Forms.TextBox();
            this.imgSinger = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgSinger)).BeginInit();
            this.SuspendLayout();
            // 
            // lvSingerInfo
            // 
            this.lvSingerInfo.FullRowSelect = true;
            this.lvSingerInfo.GridLines = true;
            this.lvSingerInfo.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvSingerInfo.HideSelection = false;
            this.lvSingerInfo.LabelWrap = false;
            this.lvSingerInfo.Location = new System.Drawing.Point(229, 35);
            this.lvSingerInfo.MultiSelect = false;
            this.lvSingerInfo.Name = "lvSingerInfo";
            this.lvSingerInfo.ShowItemToolTips = true;
            this.lvSingerInfo.Size = new System.Drawing.Size(455, 251);
            this.lvSingerInfo.TabIndex = 0;
            this.lvSingerInfo.UseCompatibleStateImageBehavior = false;
            this.lvSingerInfo.View = System.Windows.Forms.View.Details;
            // 
            // edVoiceDBFolder
            // 
            this.edVoiceDBFolder.Dock = System.Windows.Forms.DockStyle.Top;
            this.edVoiceDBFolder.Location = new System.Drawing.Point(8, 8);
            this.edVoiceDBFolder.Name = "edVoiceDBFolder";
            this.edVoiceDBFolder.Size = new System.Drawing.Size(768, 21);
            this.edVoiceDBFolder.TabIndex = 1;
            // 
            // btnSetVoiceDBFolder
            // 
            this.btnSetVoiceDBFolder.Location = new System.Drawing.Point(690, 35);
            this.btnSetVoiceDBFolder.Name = "btnSetVoiceDBFolder";
            this.btnSetVoiceDBFolder.Size = new System.Drawing.Size(83, 44);
            this.btnSetVoiceDBFolder.TabIndex = 2;
            this.btnSetVoiceDBFolder.Text = "SCAN NOW!";
            this.btnSetVoiceDBFolder.UseVisualStyleBackColor = true;
            this.btnSetVoiceDBFolder.Click += new System.EventHandler(this.btnSetVoiceDBFolder_Click);
            // 
            // tvSingers
            // 
            this.tvSingers.CheckBoxes = true;
            this.tvSingers.HideSelection = false;
            this.tvSingers.Location = new System.Drawing.Point(8, 35);
            this.tvSingers.Name = "tvSingers";
            treeNode9.Name = "V2";
            treeNode9.Text = "V2";
            treeNode10.Name = "V3";
            treeNode10.Text = "V3";
            this.tvSingers.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode9,
            treeNode10});
            this.tvSingers.ShowNodeToolTips = true;
            this.tvSingers.Size = new System.Drawing.Size(215, 519);
            this.tvSingers.TabIndex = 3;
            this.tvSingers.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.tvSingers_AfterCheck);
            this.tvSingers.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvSingers_AfterSelect);
            // 
            // btnInstall
            // 
            this.btnInstall.Location = new System.Drawing.Point(690, 116);
            this.btnInstall.Name = "btnInstall";
            this.btnInstall.Size = new System.Drawing.Size(83, 44);
            this.btnInstall.TabIndex = 4;
            this.btnInstall.Text = "Install";
            this.btnInstall.UseVisualStyleBackColor = true;
            this.btnInstall.Click += new System.EventHandler(this.btnInstall_Click);
            // 
            // btnUninstall
            // 
            this.btnUninstall.Location = new System.Drawing.Point(690, 166);
            this.btnUninstall.Name = "btnUninstall";
            this.btnUninstall.Size = new System.Drawing.Size(83, 44);
            this.btnUninstall.TabIndex = 5;
            this.btnUninstall.Text = "Uninstall";
            this.btnUninstall.UseVisualStyleBackColor = true;
            this.btnUninstall.Click += new System.EventHandler(this.btnUninstall_Click);
            // 
            // btnClean
            // 
            this.btnClean.Location = new System.Drawing.Point(690, 216);
            this.btnClean.Name = "btnClean";
            this.btnClean.Size = new System.Drawing.Size(83, 44);
            this.btnClean.TabIndex = 6;
            this.btnClean.Text = "Clean";
            this.btnClean.UseVisualStyleBackColor = true;
            this.btnClean.Click += new System.EventHandler(this.btnClean_Click);
            // 
            // btnCleanAll
            // 
            this.btnCleanAll.Location = new System.Drawing.Point(690, 510);
            this.btnCleanAll.Name = "btnCleanAll";
            this.btnCleanAll.Size = new System.Drawing.Size(83, 44);
            this.btnCleanAll.TabIndex = 7;
            this.btnCleanAll.Text = "Clean All";
            this.btnCleanAll.UseVisualStyleBackColor = true;
            this.btnCleanAll.Click += new System.EventHandler(this.btnCleanAll_Click);
            // 
            // btnInstallAll
            // 
            this.btnInstallAll.Location = new System.Drawing.Point(689, 410);
            this.btnInstallAll.Name = "btnInstallAll";
            this.btnInstallAll.Size = new System.Drawing.Size(83, 44);
            this.btnInstallAll.TabIndex = 8;
            this.btnInstallAll.Text = "Install All";
            this.btnInstallAll.UseVisualStyleBackColor = true;
            this.btnInstallAll.Click += new System.EventHandler(this.btnInstallAll_Click);
            // 
            // btnUninstallAll
            // 
            this.btnUninstallAll.Location = new System.Drawing.Point(689, 460);
            this.btnUninstallAll.Name = "btnUninstallAll";
            this.btnUninstallAll.Size = new System.Drawing.Size(83, 44);
            this.btnUninstallAll.TabIndex = 9;
            this.btnUninstallAll.Text = "Uninstall All";
            this.btnUninstallAll.UseVisualStyleBackColor = true;
            this.btnUninstallAll.Click += new System.EventHandler(this.btnUninstallAll_Click);
            // 
            // edSingerIntro
            // 
            this.edSingerIntro.Location = new System.Drawing.Point(229, 292);
            this.edSingerIntro.Multiline = true;
            this.edSingerIntro.Name = "edSingerIntro";
            this.edSingerIntro.ReadOnly = true;
            this.edSingerIntro.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.edSingerIntro.Size = new System.Drawing.Size(252, 262);
            this.edSingerIntro.TabIndex = 10;
            // 
            // imgSinger
            // 
            this.imgSinger.Location = new System.Drawing.Point(487, 292);
            this.imgSinger.Name = "imgSinger";
            this.imgSinger.Size = new System.Drawing.Size(196, 261);
            this.imgSinger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgSinger.TabIndex = 11;
            this.imgSinger.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.imgSinger);
            this.Controls.Add(this.edSingerIntro);
            this.Controls.Add(this.btnUninstallAll);
            this.Controls.Add(this.btnInstallAll);
            this.Controls.Add(this.btnCleanAll);
            this.Controls.Add(this.btnClean);
            this.Controls.Add(this.btnUninstall);
            this.Controls.Add(this.btnInstall);
            this.Controls.Add(this.tvSingers);
            this.Controls.Add(this.btnSetVoiceDBFolder);
            this.Controls.Add(this.edVoiceDBFolder);
            this.Controls.Add(this.lvSingerInfo);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Padding = new System.Windows.Forms.Padding(8);
            this.Text = "Singer Assistant";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imgSinger)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvSingerInfo;
        private System.Windows.Forms.TextBox edVoiceDBFolder;
        private System.Windows.Forms.Button btnSetVoiceDBFolder;
        private System.Windows.Forms.TreeView tvSingers;
        private System.Windows.Forms.Button btnInstall;
        private System.Windows.Forms.Button btnUninstall;
        private System.Windows.Forms.Button btnClean;
        private System.Windows.Forms.Button btnCleanAll;
        private System.Windows.Forms.Button btnInstallAll;
        private System.Windows.Forms.Button btnUninstallAll;
        private System.Windows.Forms.TextBox edSingerIntro;
        private System.Windows.Forms.PictureBox imgSinger;
    }
}

