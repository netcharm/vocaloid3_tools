﻿namespace HanziToPinyin
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.grpInput = new System.Windows.Forms.GroupBox();
            this.edInput = new System.Windows.Forms.TextBox();
            this.grpOutput = new System.Windows.Forms.GroupBox();
            this.edOutput = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.grpOption = new System.Windows.Forms.GroupBox();
            this.chkInsertSpace = new System.Windows.Forms.CheckBox();
            this.radioOptionUpper = new System.Windows.Forms.RadioButton();
            this.radioOptionLower = new System.Windows.Forms.RadioButton();
            this.radioOptionCaps = new System.Windows.Forms.RadioButton();
            this.btnConvert = new System.Windows.Forms.Button();
            this.dlgSaveAs = new System.Windows.Forms.SaveFileDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbEngine = new System.Windows.Forms.ComboBox();
            this.grpInput.SuspendLayout();
            this.grpOutput.SuspendLayout();
            this.panel1.SuspendLayout();
            this.grpOption.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpInput
            // 
            this.grpInput.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.grpInput.Controls.Add(this.edInput);
            this.grpInput.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpInput.Location = new System.Drawing.Point(8, 8);
            this.grpInput.Margin = new System.Windows.Forms.Padding(10);
            this.grpInput.Name = "grpInput";
            this.grpInput.Padding = new System.Windows.Forms.Padding(8);
            this.grpInput.Size = new System.Drawing.Size(768, 233);
            this.grpInput.TabIndex = 3;
            this.grpInput.TabStop = false;
            this.grpInput.Text = "Input";
            // 
            // edInput
            // 
            this.edInput.AcceptsReturn = true;
            this.edInput.AcceptsTab = true;
            this.edInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.edInput.HideSelection = false;
            this.edInput.ImeMode = System.Windows.Forms.ImeMode.On;
            this.edInput.Location = new System.Drawing.Point(8, 22);
            this.edInput.Multiline = true;
            this.edInput.Name = "edInput";
            this.edInput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.edInput.Size = new System.Drawing.Size(752, 203);
            this.edInput.TabIndex = 1;
            this.edInput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.edInput_KeyDown);
            // 
            // grpOutput
            // 
            this.grpOutput.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.grpOutput.Controls.Add(this.edOutput);
            this.grpOutput.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grpOutput.Location = new System.Drawing.Point(8, 298);
            this.grpOutput.Name = "grpOutput";
            this.grpOutput.Padding = new System.Windows.Forms.Padding(8);
            this.grpOutput.Size = new System.Drawing.Size(768, 256);
            this.grpOutput.TabIndex = 4;
            this.grpOutput.TabStop = false;
            this.grpOutput.Text = "Output";
            // 
            // edOutput
            // 
            this.edOutput.AcceptsReturn = true;
            this.edOutput.AcceptsTab = true;
            this.edOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.edOutput.HideSelection = false;
            this.edOutput.Location = new System.Drawing.Point(8, 22);
            this.edOutput.Multiline = true;
            this.edOutput.Name = "edOutput";
            this.edOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.edOutput.Size = new System.Drawing.Size(752, 226);
            this.edOutput.TabIndex = 1;
            this.edOutput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.edOutput_KeyDown);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.grpOption);
            this.panel1.Controls.Add(this.btnConvert);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(8, 241);
            this.panel1.Margin = new System.Windows.Forms.Padding(16, 3, 16, 3);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(4);
            this.panel1.Size = new System.Drawing.Size(768, 57);
            this.panel1.TabIndex = 2;
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Location = new System.Drawing.Point(574, 4);
            this.btnSave.Margin = new System.Windows.Forms.Padding(16, 3, 16, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Padding = new System.Windows.Forms.Padding(8);
            this.btnSave.Size = new System.Drawing.Size(95, 49);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // grpOption
            // 
            this.grpOption.AutoSize = true;
            this.grpOption.Controls.Add(this.chkInsertSpace);
            this.grpOption.Controls.Add(this.radioOptionUpper);
            this.grpOption.Controls.Add(this.radioOptionLower);
            this.grpOption.Controls.Add(this.radioOptionCaps);
            this.grpOption.Dock = System.Windows.Forms.DockStyle.Left;
            this.grpOption.Location = new System.Drawing.Point(4, 4);
            this.grpOption.Margin = new System.Windows.Forms.Padding(16, 3, 3, 3);
            this.grpOption.Name = "grpOption";
            this.grpOption.Padding = new System.Windows.Forms.Padding(8, 3, 3, 3);
            this.grpOption.Size = new System.Drawing.Size(324, 49);
            this.grpOption.TabIndex = 4;
            this.grpOption.TabStop = false;
            this.grpOption.Text = "Options";
            // 
            // chkInsertSpace
            // 
            this.chkInsertSpace.AutoSize = true;
            this.chkInsertSpace.Checked = true;
            this.chkInsertSpace.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkInsertSpace.Dock = System.Windows.Forms.DockStyle.Left;
            this.chkInsertSpace.Location = new System.Drawing.Point(209, 17);
            this.chkInsertSpace.Name = "chkInsertSpace";
            this.chkInsertSpace.Padding = new System.Windows.Forms.Padding(8, 4, 8, 4);
            this.chkInsertSpace.Size = new System.Drawing.Size(112, 29);
            this.chkInsertSpace.TabIndex = 3;
            this.chkInsertSpace.Text = "Insert Space";
            this.chkInsertSpace.UseVisualStyleBackColor = true;
            // 
            // radioOptionUpper
            // 
            this.radioOptionUpper.AutoSize = true;
            this.radioOptionUpper.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioOptionUpper.Location = new System.Drawing.Point(140, 17);
            this.radioOptionUpper.Margin = new System.Windows.Forms.Padding(8, 3, 8, 3);
            this.radioOptionUpper.Name = "radioOptionUpper";
            this.radioOptionUpper.Padding = new System.Windows.Forms.Padding(8, 4, 8, 4);
            this.radioOptionUpper.Size = new System.Drawing.Size(69, 29);
            this.radioOptionUpper.TabIndex = 2;
            this.radioOptionUpper.Text = "Upper";
            this.radioOptionUpper.UseVisualStyleBackColor = true;
            // 
            // radioOptionLower
            // 
            this.radioOptionLower.AutoSize = true;
            this.radioOptionLower.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioOptionLower.Location = new System.Drawing.Point(71, 17);
            this.radioOptionLower.Margin = new System.Windows.Forms.Padding(8, 3, 8, 3);
            this.radioOptionLower.Name = "radioOptionLower";
            this.radioOptionLower.Padding = new System.Windows.Forms.Padding(8, 4, 8, 4);
            this.radioOptionLower.Size = new System.Drawing.Size(69, 29);
            this.radioOptionLower.TabIndex = 1;
            this.radioOptionLower.Text = "Lower";
            this.radioOptionLower.UseVisualStyleBackColor = true;
            // 
            // radioOptionCaps
            // 
            this.radioOptionCaps.AutoSize = true;
            this.radioOptionCaps.Checked = true;
            this.radioOptionCaps.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioOptionCaps.Location = new System.Drawing.Point(8, 17);
            this.radioOptionCaps.Margin = new System.Windows.Forms.Padding(8, 3, 8, 3);
            this.radioOptionCaps.Name = "radioOptionCaps";
            this.radioOptionCaps.Padding = new System.Windows.Forms.Padding(8, 4, 8, 4);
            this.radioOptionCaps.Size = new System.Drawing.Size(63, 29);
            this.radioOptionCaps.TabIndex = 0;
            this.radioOptionCaps.TabStop = true;
            this.radioOptionCaps.Text = "CAPS";
            this.radioOptionCaps.UseVisualStyleBackColor = true;
            // 
            // btnConvert
            // 
            this.btnConvert.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnConvert.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnConvert.Location = new System.Drawing.Point(669, 4);
            this.btnConvert.Margin = new System.Windows.Forms.Padding(16, 3, 16, 3);
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.Padding = new System.Windows.Forms.Padding(8);
            this.btnConvert.Size = new System.Drawing.Size(95, 49);
            this.btnConvert.TabIndex = 3;
            this.btnConvert.Text = "Convert";
            this.btnConvert.UseVisualStyleBackColor = true;
            this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // dlgSaveAs
            // 
            this.dlgSaveAs.DefaultExt = "txt";
            this.dlgSaveAs.FileName = "*.txt";
            this.dlgSaveAs.Filter = "Text File(*.txt)|*.txt";
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox1.Controls.Add(this.cbEngine);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(328, 4);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(12, 3, 3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(8, 3, 8, 3);
            this.groupBox1.Size = new System.Drawing.Size(108, 49);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Engine";
            // 
            // cbEngine
            // 
            this.cbEngine.Dock = System.Windows.Forms.DockStyle.Left;
            this.cbEngine.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEngine.FormattingEnabled = true;
            this.cbEngine.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.cbEngine.Items.AddRange(new object[] {
            "NPinyin",
            "Simple"});
            this.cbEngine.Location = new System.Drawing.Point(8, 17);
            this.cbEngine.Name = "cbEngine";
            this.cbEngine.Size = new System.Drawing.Size(92, 20);
            this.cbEngine.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.grpOutput);
            this.Controls.Add(this.grpInput);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "MainForm";
            this.Padding = new System.Windows.Forms.Padding(8);
            this.Text = "Hanzi To Pinyin";
            this.grpInput.ResumeLayout(false);
            this.grpInput.PerformLayout();
            this.grpOutput.ResumeLayout(false);
            this.grpOutput.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.grpOption.ResumeLayout(false);
            this.grpOption.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpInput;
        private System.Windows.Forms.TextBox edInput;
        private System.Windows.Forms.GroupBox grpOutput;
        private System.Windows.Forms.TextBox edOutput;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox grpOption;
        private System.Windows.Forms.RadioButton radioOptionUpper;
        private System.Windows.Forms.RadioButton radioOptionLower;
        private System.Windows.Forms.RadioButton radioOptionCaps;
        private System.Windows.Forms.CheckBox chkInsertSpace;
        private System.Windows.Forms.SaveFileDialog dlgSaveAs;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnConvert;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbEngine;
    }
}

