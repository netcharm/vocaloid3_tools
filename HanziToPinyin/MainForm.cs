﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using NPinyin;

namespace HanziToPinyin
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            this.Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);
            cbEngine.SelectedIndex = 0;
        }

        private void btnConvert_Click( object sender, EventArgs e )
        {
            if ( edInput.Text.Length > 0 )
            {
                String hzText = edInput.Text.Trim();
                int hzLen = hzText.Length;
                Boolean insertSpace = chkInsertSpace.Checked;

                hz2py.StringMode StringCase = hz2py.StringMode.Caps;
                if ( radioOptionCaps.Checked ) StringCase = hz2py.StringMode.Caps;
                if ( radioOptionLower.Checked ) StringCase = hz2py.StringMode.LowCase;
                if ( radioOptionUpper.Checked ) StringCase = hz2py.StringMode.UpCase;

                switch ( cbEngine.SelectedIndex )
                {
                    case 1:
                        edOutput.Text = hz2py.Convert( hzText, hzLen, insertSpace, StringCase );
                        break;
                    case 0:
                        //edOutput.Text = Pinyin.GetPinyin( edInput.Text );
                        edOutput.Clear();
                        TextInfo pyWord = new CultureInfo( "en-US", false ).TextInfo;
                        String pyString = String.Empty;
                        String pyValue = String.Empty;

                        foreach ( string line in edInput.Lines )
                        {
                            //edOutput.AppendText( Pinyin.GetPinyin( line ) + Environment.NewLine );

                            pyString = Pinyin.GetPinyin( line );
                            if ( StringCase == hz2py.StringMode.LowCase )
                            {
                                pyValue = pyWord.ToLower( pyString ).Trim();
                            }
                            else if ( StringCase == hz2py.StringMode.UpCase )
                            {
                                pyValue = pyWord.ToUpper( pyString ).Trim();
                            }
                            else if ( StringCase == hz2py.StringMode.Caps )
                            {
                                pyValue = pyWord.ToTitleCase( pyString ).Trim();
                            }
                            edOutput.AppendText( pyValue + Environment.NewLine );
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private void edInput_KeyDown( object sender, KeyEventArgs e )
        {
            if ((e.Modifiers == Keys.Control) && (e.KeyCode == Keys.A))
            {
                edInput.SelectAll();
                e.SuppressKeyPress = true;
            }
        }

        private void edOutput_KeyDown( object sender, KeyEventArgs e )
        {
            if ( ( e.Modifiers == Keys.Control ) && ( e.KeyCode == Keys.A ) )
            {
                edOutput.SelectAll();
                e.SuppressKeyPress = true;
            }
        }

        private void btnSave_Click( object sender, EventArgs e )
        {
            if ( this.edOutput.Text.Length <= 0 ) return;

            if ( dlgSaveAs.ShowDialog() == DialogResult.OK )
            {
                StreamWriter sw = new StreamWriter( dlgSaveAs.FileName, false, Encoding.UTF8 );//文件路径，是否覆盖指定路径同名文件，文本编码

                sw.Write( this.edInput.Text );

                sw.Write( Environment.NewLine );
                sw.Write( Environment.NewLine );
                sw.Write( "====================================================" );
                sw.Write( Environment.NewLine );
                sw.Write( Environment.NewLine );

                sw.Write( this.edOutput.Text );

                sw.Close();
            }
        }
    }
}
