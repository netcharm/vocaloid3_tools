﻿These software are freeware and no warranty.
License is APACHE LICENSE 2.0.

Intro:
======
You can clone all source code.

HanziToPinyin
====================
  Convert chinese text to chinese latin pinyin. It using NPinyin library & other man's code

SingerAssistant
====================
  Help you to manager singer install/uninstall/clean item in reg, and it may display basic info
of singer by user define *.info file. You can define a new info file with new product on feature.

SingerInfos
====================
  Some pre-define singer info file for user. you can download it and unpack it to your voicedb,
and move to singer's installed folder with right hashname.
  etc: Tianyi_CHN's hashname is BETDB8W6KWZPYEB9, so
  put BETDB8W6KWZPYEB9.info -> Luo_Tianyi's installed folder.
